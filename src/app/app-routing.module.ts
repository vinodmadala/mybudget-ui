import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import {LoginComponent} from './modules/authentication/login/login.component';
import {RegisterComponent} from './modules/authentication/register/register.component';
import {CalendarComponent} from './modules/full-calendar/calendar.component';
import {AuthGuard} from './auth/auth.guard';
import {UserProfileComponent} from './modules/user-profile/user-profile.component';
import {MapComponent} from './modules/map/map.component';

const routes: Routes = [{
  path: 'dashboard',
  component: DefaultComponent,
  canActivate: [AuthGuard],
  children: [{
    path: 'statistics',
    component: DashboardComponent,
    data: {
      title: 'Dashboard',
      urls: [{title: 'Dashboard', url: '/dashboard/statics'}]
    }
  },
    {
      path: 'calendar',
      component: CalendarComponent,
      data: {
        title: 'Calendar',
        urls: [{title: 'Dashboard', url: '/dashboard/statistics'},{title: 'Calendar', url: '/dashboard/calendar'}]
      }
    },
    {
      path: 'map',
      component: MapComponent,
      data: {
        title: 'Map',
        urls: [{title: 'Dashboard', url: '/dashboard/statistics'},{title: 'Map', url: '/dashboard/map'}]
      }
    },
    {
      path: 'user-profile',
      component: UserProfileComponent,
      data: {
        title: 'Profile',
        urls: [{title: 'Dashboard', url: '/dashboard/statistics'},{title: 'Profile', url: '/dashboard/user-profile'}]
      }
    }
  ]
},
  {
    path: 'authentication',
    component: LoginComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  },
  {
    path: 'authentication/register',
    component: RegisterComponent
  },
  {
    path: '',
    redirectTo: 'authentication/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'authentication/login',
    pathMatch: 'full'
  }
 ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
