import { Component, OnInit } from '@angular/core';
interface Gender {
  value: number;
  viewValue: string;

}
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  fileData: File = null;
  previewUrl: any = '../../../assets/person.png';
  constructor() { }

  genderList: Gender[] = [
    {value: 0, viewValue: 'Male'},
    {value: 1, viewValue: 'Female'},
  ];

  ngOnInit() {
  }

  fileProgress(fileInput: any) {
    console.log('--------calling----------')
    this.fileData = <File> fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview
    // tslint:disable-next-line:prefer-const
    let mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    // tslint:disable-next-line:prefer-const
    let reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (event) => {
      this.previewUrl = reader.result;
    };
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.fileData);
    console.log('-------form data----------', JSON.stringify(formData));
    // this.http.post('url/to/your/api', formData)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.uploadedFilePath = res.data.filePath;
    //     alert('SUCCESS !!');
    //   })
  }

}
