import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
// import custom validator to validate that password and confirm password fields match
import { AuthService } from 'angularx-social-login';
// Import angular social login providers
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import {BsDatepickerConfig} from 'ngx-bootstrap';
import {UserService} from '../../../services/user.service';
import {ToastrService} from 'ngx-toastr';
import {MustMatch} from '../../../_helpers/confirm-password.validator';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  datepickerConfig: Partial<BsDatepickerConfig>;

  private user: {};
  private loggedIn: boolean;

  constructor(private userService: UserService, private toastr: ToastrService, private router: Router,
              private formBuilder: FormBuilder,
              private socialAuthService: AuthService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: true,
      // minDate: new Date(1960, 0,1),
      dateInputFormat: 'DD/MM/YYYY'

    });
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      // tslint:disable-next-line:max-line-length
      mobileNumber: ['', [Validators.required, Validators.pattern('^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$')]],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$')]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    this.socialAuthService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });

  }
  get f() {
    return this.registerForm.controls;
  }

  signup() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    console.log('----------registeration form data--------');
    const registerData = {
      firstName : this.f.firstName.value,
      lastName : this.f.lastName.value,
      email : this.f.email.value,
      mobileNumber : this.f.mobileNumber.value,
      password : this.f.password.value,
    };

    console.log(JSON.stringify(registerData));

    this.userService.registerUser(registerData)
      .subscribe((data: any) => {
          if (data.Succeeded === true) {
            this.toastr.success('User registration successful');
            this.router.navigate(['/authentication/login']);
          } else {
            this.toastr.error('User already existed with that email!');
          }
        }, (error: any) =>
          console.log(error)
      );
  }

  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  signInWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signOut(): void {
    this.socialAuthService.signOut();
  }


}
