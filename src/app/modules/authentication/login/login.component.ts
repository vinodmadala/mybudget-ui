import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../../services/authentication.service';
import {ToastrService} from 'ngx-toastr';
import {MustMatch} from '../../../_helpers/confirm-password.validator';
import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']

})
export class LoginComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  forgotForm: FormGroup;
  resetForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  returnUrl: string;
  error = '';

  constructor(private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService,
              private spinner: NgxSpinnerService) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/dashboard/statistics']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]]
    });

    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]]
    });

    this.resetForm = this.formBuilder.group({
      otp: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$')]],
      confirmNewPassword: ['', [Validators.required]],
    }, {
      validator: MustMatch('newPassword', 'confirmNewPassword')
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/dashboard/statistics';
    localStorage.clear();
  }

  ngAfterViewInit() {
    $('#to-recover').on('click', () => {
      this.loginForm.reset();
      $('#loginForm').slideUp();
      $('#recoverForm').fadeIn();
    });
  }

  get f() {
    return this.loginForm.controls;
  }
  get f1() {
    return this.forgotForm.controls;
  }
  get f2() {
    return this.resetForm.controls;
  }

  restBack() {
    this.forgotForm.reset();
    this.resetForm.reset();
    $('#resetPasswordForm').slideUp();
    $('#loginForm').fadeIn();
  }

  login() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toastr.success('Susccessfully Loggedin!');
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.toastr.warning('Oops Invalid Credentials!');
          this.error = error;
          this.loading = false;
        });
  }

  register() {
    this.router.navigate(['/authentication/register']);
  }

  loginPage() {
    this.forgotForm.reset();
    this.resetForm.reset();
    $('#loginForm').fadeIn();
    $('#recoverForm').slideUp();

  }
  forgotPassword() {
    if (this.forgotForm.invalid) {
      return;
    }
    this.spinner.show();
    this.authenticationService.forgotPassword(this.f1.email.value).subscribe(
      (data: any) => {
          console.log('---------data from beackend ---------' + JSON.stringify(data));
          if (data.status === 200) {
            this.spinner.hide();
            localStorage.setItem('userId', data.userId)
            this.toastr.success(data.message);
            $('#recoverForm').slideUp();
            $('#resetPasswordForm').fadeIn();
          } else {
            this.spinner.hide()
            this.toastr.warning(data.message);
          }
        },
        error => {
          this.spinner.hide();
          this.toastr.error(error);
          this.error = error;
          this.loading = false;
        });
  }

  resetPassword() {
    if (this.resetForm.valid) {
      const resetData = {
        userId: localStorage.getItem('userId'),
        newPassword: this.resetForm.value.newPassword,
        otp: this.resetForm.value.otp
      }
      this.authenticationService.resetPassword(resetData).subscribe(data => {
        if (data.status === 200) {
          this.toastr.success(data.message);
          this.resetForm.reset();
          $('#resetPasswordForm').slideUp();
          $('#loginForm').fadeIn();
        } else {
          this.toastr.warning(data.message);
        }
      });
    }
  }



}
