import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DefaultModule } from './layouts/default/default.module';
import {AuthenticationModule} from './modules/authentication/authentication.module';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {BsDatepickerModule} from 'ngx-bootstrap';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CalendarComponent } from './modules/full-calendar/calendar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { UserProfileComponent } from './modules/user-profile/user-profile.component';
import {MaterialModule} from './shared/material.module';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { MapComponent } from './modules/map/map.component';
import { AgmCoreModule } from '@agm/core';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelSpeed: 2,
  wheelPropagation: true,
  suppressScrollX: true
};


@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    UserProfileComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DefaultModule,
    AuthenticationModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    FullCalendarModule, // for FullCalendar!
    NgxSpinnerModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAg3mDReK0SZvie62gT14P5vRy9oRouMFE',
      libraries: ['places']
    })
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }, {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



